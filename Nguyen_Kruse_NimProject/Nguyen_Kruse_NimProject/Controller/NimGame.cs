﻿using Nguyen_Kruse_NimProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Controller
{
    class NimGame
    {
        public Player PlayerOne { get; set; }
        public Player PlayerTwo { get; set; }
        public Board Board { get; set; }
        public GameState GameState = null;
        bool isPlayerOneTurn = true;
        Player currentPlayer = null;
        AI _AI = null;

        public NimGame(Player playerone, Player playertwo, AI ai)
        {
            PlayerOne = playerone;
            PlayerTwo = playertwo;
            this.Board = new Board();
            GameState = new GameState(this.Board);
            _AI = ai;
            _AI._GameState = this.GameState;
        }

        public void PlayGame()
        {
            bool hasEnded = false;
            while (!hasEnded)
            {
                currentPlayer = isPlayerOneTurn ? PlayerOne : PlayerTwo;
                PlayRound();
                hasEnded = Board.getTotalTokens() == 0 ? true : false;
                GameState.RecordBoardState(this.Board);
                isPlayerOneTurn = !isPlayerOneTurn;
                if (hasEnded) break;
                
            }
            Player winner = isPlayerOneTurn ? PlayerTwo : PlayerOne; //reverse logic b/c whoever's turn it was last, loses
            Console.WriteLine(currentPlayer.Name + " WINS!!");
        }

        public void PlayRound()
        {
            int rowToTake = 0;
            int tokenCountToTake = 0;
            
            try
            {
                Console.WriteLine(Board.ToString());
                
                currentPlayer.PlayTurn();
                
                rowToTake = currentPlayer.ChooseRow();
                tokenCountToTake = currentPlayer.ChooseAmountToTake();

                Board.TakeTokens(rowToTake, tokenCountToTake);

                
                Console.WriteLine(Board.ToString());
            }
            catch (FormatException e)
            {
                Console.WriteLine("Bad input.... Try again.");
                PlayRound();
            }


        }


         




    }
}
