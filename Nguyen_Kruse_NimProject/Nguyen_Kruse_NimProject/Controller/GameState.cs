﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nguyen_Kruse_NimProject.Models;

namespace Nguyen_Kruse_NimProject
{
    class GameState
    {
        Board _Board = null;
        public List<Board> BoardState = new List<Board>();

        public GameState(Board board) {
            _Board = board;
        }

        public int getRowCountById(int id)
        {
            Row row = _Board.rowArray[id];
            return row.Count;
        }

        public bool isRowEmptyById(int id)
        {
            Row row = _Board.rowArray[id];
            return row.Count == 0;
        }

        public void RecordBoardState(Board board){
            BoardState.Add(board);
        }

    }
}
