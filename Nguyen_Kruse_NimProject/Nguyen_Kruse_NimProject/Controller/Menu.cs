﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary;
using Nguyen_Kruse_NimProject.Controller;

namespace Nguyen_Kruse_NimProject.Models
{
    /**
    * CIO is a helper library containing prompts(Menu, input, bool, int, ect...)
    **/
    class Menu
    {

        List<String> options = new List<string> { "Player vs Player", "Player vs Computer", "Computer vs Computer" };
        
        Player player1 = null; //unset players
        Player player2 = null;
        AI _AI = null;
        //delegate void MakeGame(Player player1, Player player2);
        public Menu()
        {
            _AI = new AI();
        }

        public void Start()
        {
            
            bool isPlaying = true;
            int response;
            while (isPlaying)
            {
                response = CIO.PromptForMenuSelection(this.options, true);  //CIO is the helper library imported (for menu)
                if (response == 0) Environment.Exit(0);
                else if (response == 1)
                {
                    player1 = new Person();
                    player2 = new Person();
                }
                else if (response == 2)
                {
                    player1 = new Person();
                    player2 = new Computer(_AI);
                }
                else if (response == 3)
                {
                    player1 = new Computer(_AI);
                    player2 = new Computer(_AI);
                }
                NimGame nim = new NimGame(player1, player2, _AI);
                nim.PlayGame();
                
            }
        }

        public void MakeGame(Player playerOne, Player playerTwo)
        {
            NimGame nim = new NimGame(playerOne, playerTwo, _AI);
            nim.PlayGame();
        }

        public void StartPVPGame()
        {

        }

        public void StartPVCGame()
        {

        }

        public void StartCVCGame()
        {

        }


    }

}
