﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Models
{
    abstract class Player
    {

        public abstract String Name { get; set; }

        public abstract void PlayTurn();
        public abstract int ChooseRow();
        public abstract int ChooseAmountToTake();
    }
}
