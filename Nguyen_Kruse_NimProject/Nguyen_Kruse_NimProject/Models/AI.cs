﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nguyen_Kruse_NimProject.Models;

namespace Nguyen_Kruse_NimProject
{
    class AI
    {
        public GameState _GameState { get; set; }
        public Dictionary<Score, int> Intelligence
        {
            get;
            set;
        }

        //void DetermineIntelligenceValues()
        //{
        //    foreach (Board b in _GameState.BoardState)
        //    {
        //        b._Score.scoreArray;
        //    }
        //}

        public int ChooseRandomRow()
        {
            List<int> rows = new List<int>();
            Board board = null;
            if (_GameState.BoardState.Count == 0)
            {
                board = new Board();
            }
            else
            {
                board = _GameState.BoardState[(_GameState.BoardState.Count - 1)];
            }

            for (int i = 0; i < board.rowArray.Length; i++)
            {
                if (!_GameState.isRowEmptyById(i)) rows.Add(i);
            }
            Random randomRow = new Random();
            int row = randomRow.Next(3);
            if (!rows.Contains(row)) row = ChooseRandomRow();

            return row;
        }

        public int ChooseRandomAmount(int row)
        {
            Board board = null;
            if (_GameState.BoardState.Count == 0)
            {
                board = new Board();
            }
            else
            {
                board = _GameState.BoardState[(_GameState.BoardState.Count - 1)];
            }
            int numOfTokens = board._Score.statusArray[row];
            Random rand = new Random();
            int chosenAmount = rand.Next(1, numOfTokens);

            return chosenAmount;
        }

    }
}
