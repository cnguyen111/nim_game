﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Models
{
    class Row
    {
        //public Token Token {   get;set;    }

        public int Count { get; set; }

        public Row(int countTokens){
            Count = countTokens;
        }

        public void takeFromRow(int subtractTokens){
           
            Count = Count - subtractTokens;

        }

        public override string ToString()
        {
            string tokens = "";
            for (int x = 0; x < Count; x++)
            {
                tokens +="x ";
            }
                return tokens;
        }

        public Row getRow()
        {
            return this;
        }
    }
}
