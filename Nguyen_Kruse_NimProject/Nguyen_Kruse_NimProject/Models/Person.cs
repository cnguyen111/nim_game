﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Models
{
    class Person:Player
    {
        public override String Name { get; set; }
        static int id = 0;
        public Person()
        {
           id++;
           Name = "Human Player " + id;
        }

        public override void PlayTurn()
        {
            Console.WriteLine(Name + "'s turn");
        }

        public override int ChooseRow()
        {
            int rowToTake = 0;
            try
            {
                Console.WriteLine("Which row would you like to take from?");
                rowToTake = int.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                throw new FormatException();
            }

            return rowToTake-1;
        }

        public override int ChooseAmountToTake()
        {
            int tokenCountToTake = 0;
            try
            {
                Console.WriteLine("How many tokens do you want to take?");
                tokenCountToTake = int.Parse(Console.ReadLine());

            }
            catch (FormatException e)
            {
                throw new FormatException();
            }

            return tokenCountToTake;
        }
    }
}
