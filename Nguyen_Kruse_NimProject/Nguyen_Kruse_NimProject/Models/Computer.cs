﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Models
{
    class Computer: Player
    {

        public override String Name { get; set; }
        public int ChosenRow { get; set; }
        GameState _GameState = null;
        AI _AI = null;
        
        static int id = 0;
        public Computer(AI ai)
        {
            _AI = ai;
            id++;
            Name = "Computer Player " + id;
        }
        public override void PlayTurn()
        {
            int[] input = new int[1];
            Console.WriteLine(Name + ": Hold on, I am thinking...");
            makeMove();
        }

        public override int ChooseRow()
        {
            ChosenRow = _AI.ChooseRandomRow();
            return ChosenRow;
        }

        public override int ChooseAmountToTake()
        {
            return _AI.ChooseRandomAmount(ChosenRow);
        }

        private int[] makeMove() //WIll eventually return an int[] for rows and tokens
        {
            int[] input = null;
     

            return input;
        }

    }
}
