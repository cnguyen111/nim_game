﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nguyen_Kruse_NimProject.Models
{
    class Board
    {
        private const int ROWCOUNT = 3;
        public const int FIRST_ROW_TOKENS = 3;
        public const int SECOND_ROW_TOKENS = 5;
        public const int THIRD_ROW_TOKENS = 7;
        public Row[] rowArray = new Row[ROWCOUNT];
        public Score _Score { get; set; }

        public Board()
        {

            rowArray[0] = new Row(FIRST_ROW_TOKENS);
            rowArray[1] = new Row(SECOND_ROW_TOKENS);
            rowArray[2] = new Row(THIRD_ROW_TOKENS);
            _Score = new Score(rowArray);
        }

        public void TakeTokens(int row, int howMany)
        {
            try
            {
                rowArray[row].takeFromRow(howMany);
                _Score = new Score(rowArray);
            }
            catch(Exception e){
                Console.WriteLine("Row does not contain that amount of tokens.");
            }
        }
        
        //This method needs to be removed into the GameState class
        public int getTotalTokens()
        {
            int totalTokens = 0;
            for (int i = 0; i < rowArray.Length; i++)
            {
                totalTokens += rowArray[i].Count;
            }
            return totalTokens;
        }

        public override string ToString()
        {
            string toReturn = "";

            for (int x = 0; x < rowArray.Length; x++)
            {
                
                toReturn += rowArray[x].ToString() + "\n";
            }
                return toReturn;
        }





    }
}
