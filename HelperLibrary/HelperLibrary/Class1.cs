﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary
{
    public static class CIO
    {
        public static int PromptForMenuSelection(IEnumerable options, bool withQuit)
        {
            bool valid = false;
            int selection = -1;
            int current = 0;

            while (!valid)
            {

                Console.WriteLine("Please Choose a Selection: ");
                foreach (String item in options)
                {
                    current++;
                    Console.WriteLine(current + "-" + item.ToString());
                }

                if (withQuit)
                {

                    Console.WriteLine("Press 0 to quit");
                    current += 1;

                }

                string input = Console.ReadLine();
                try
                {
                    selection = int.Parse(input);
                    
                    if (withQuit)
                    {
                        if (selection > current || selection < 0)
                        {
                            throw new FormatException();
                        }
                        else
                        {
                            valid = true;
                        }

                    }
                    else if (selection > current || selection < 1)
                    {
                        throw new FormatException();
                    }
                    else
                    {
                        valid = true;
                    }
                }
                catch (FormatException)
                {

                    Console.WriteLine("Please enter a valid integer selection");
                    valid = false;
                    current = 0;
                    selection = -1;
                }
            }
            return selection;


            //throw new NotImplementedException();
        }

        public static bool PromptForBool(string message, string trueString, string falseString)
        {
            bool valid = false;
            bool response = false;

            while (!valid)
            {
                Console.WriteLine(message + "\n" + trueString + "\n" + falseString);
                string input = Console.ReadLine().ToLower();

                if (input.Equals(trueString.ToLower()))
                {
                    response = true;
                    valid = true;
                }
                else if (input.Equals(falseString.ToLower()))
                {
                    response = false;
                    valid = true;
                }

                else
                {
                    Console.WriteLine("Please Enter a Valid Response");
                }
            }

            return response;

            //throw new NotImplementedException();
        }

        public static byte PromptForByte(string message, byte min, byte max)
        {
            bool valid = false;
            byte response = 0;
            if (min > max)
            {
                byte tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = byte.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }
                
            }

            return response;
            //throw new NotImplementedException();
        }

        public static short PromptForShort(string message, short min, short max)
        {
            bool valid = false;
            short response = 0;
            if (min > max)
            {
                short tempMax = max;
                max = min;
                min = tempMax;
                
            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = short.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }

            return response;
            //throw new NotImplementedException();
        }

        public static int PromptForInt(string message, int min, int max)
        {
            bool valid = false;
            int response = 0;
            if (min > max)
            {
                int tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = int.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }

            return response;
            //throw new NotImplementedException();
        }

        public static long PromptForLong(string message, long min, long max)
        {
            bool valid = false;
            long response = 0;
            if (min > max)
            {
                long tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = long.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }

            return response;
            //throw new NotImplementedException();
        }

        public static float PromptForFloat(string message, float min, float max)
        {

            bool valid = false;
            float response = 0;
            if (min > max)
            {
                float tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = float.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }
            return response;
            //throw new NotImplementedException();
        }

        public static double PromptForDouble(string message, double min, double max)
        {
        
            bool valid = false;
            double response = 0;
            if (min > max)
            {
                double tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = double.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }
            return response;
            //throw new NotImplementedException();
        }

        public static decimal PromptForDecimal(string message, decimal min, decimal max)
        {
            bool valid = false;
            decimal response = 0;
            if (min > max)
            {
                decimal tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = decimal.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }
            return response;
            //throw new NotImplementedException();
        }

        public static string PromptForInput(string message, bool allowEmpty)
        {
          bool valid = false;
          string response = null;

            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = true;
                response = input;
                if (!allowEmpty)
                {
                    if (input.Equals(""))
                    {
                        Console.WriteLine("Cannot Be Empty");
                        valid = false;
                    }
                    
                }
                
            }

            return response;
            //throw new NotImplementedException();
        }

        public static char PromptForChar(string message, char min, char max)
        {
            bool valid = false;
            char response = '0';
            if (min > max)
            {
                char tempMax = max;
                max = min;
                min = tempMax;

            }
            while (!valid)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                valid = char.TryParse(input, out response);
                if (!valid)
                {
                    Console.WriteLine("Please enter a valid input");
                }
                else
                {
                    if (response > max || response < min)
                    {
                        Console.WriteLine("Out of Range");
                        valid = false;
                    }
                }

            }
            return response;
            //throw new NotImplementedException();
        }
    }
}

